from __future__ import absolute_import
import os

from .base import *

if os.getenv('DEBUG', False):
    DEBUG = True
else:
    DEBUG = False

ALLOWED_HOSTS = ['*']  # Update this accordingly; https://docs.djangoproject.com/en/dev/ref/settings/#allowed-hosts

DATABASES = {
    'default': {
        'ENGINE': 	'django.db.backends.postgresql_psycopg2',
        'NAME': 	os.getenv('DOKKU_POSTGRES_INVENTORY_DB_NAME'),
        'USER': 	os.getenv('DOKKU_POSTGRES_INVENTORY_USER'),
        'PASSWORD': 	os.getenv('DOKKU_POSTGRES_INVENTORY_PASSWORD'),
        'HOST': 	os.getenv('DOKKU_POSTGRES_INVENTORY_PORT_5432_TCP_ADDR'),
        'PORT': 	os.getenv('DOKKU_POSTGRES_INVENTORY_PORT_5432_TCP_PORT'),
    }
}
